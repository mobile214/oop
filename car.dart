import 'dart:io';

class Car{

  String name = stdin.readLineSync()!;

  void goForward() {
    print(name+" is going straight");
  }
  void goBackward() {
    print(name+" is going reverse");
  }
  void stop() {
    print(name+" is break");
  }
  void park() {
    print(name+" is park");
  }
  void turnLeft() {
    print(name+" is turning left");
  }
  void turnRight() {
    print(name+" is turning right");
  }

}

class Honda extends Car {
  
}

class Toyota extends Car {
  
}

class Suzuki extends Car {
  
}

class Volvo extends Car {
  
}

void main() {
  var honda = new Honda();
  honda.goForward();
  honda.goBackward();
  honda.stop();
  honda.turnLeft();
  honda.turnRight();
  honda.park();

  print("****************");

  var toyota = new Toyota();
  toyota.goForward();
  toyota.goBackward();
  toyota.stop();
  toyota.turnLeft();
  toyota.turnRight();
  toyota.park();

  print("****************");

  var suzuki = new Suzuki();
  suzuki.goForward();
  suzuki.goBackward();
  suzuki.stop();
  suzuki.turnLeft();
  suzuki.turnRight();
  suzuki.park();

  print("****************");

  var volvo = new Toyota();
  volvo.goForward();
  volvo.goBackward();
  volvo.stop();
  volvo.turnLeft();
  volvo.turnRight();
  volvo.park();

  print("****************");


}
